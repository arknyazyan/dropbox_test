
window.onload = () => {
    let options = [
        {
            id: 1,
            type: "text",
            text: "option 1",
            search_text: [
                "option1",
                "1"
            ],
            tag: `<p>option 1</p>`
        },
        {
            id: 2,
            type: "text",
            text: "option 2",
            search_text: [
                "option2",
                "2"
            ],
            tag: `<p>option 2</p>`
        },
        {
            id: 3,
            type: "tag",
            text: "Google",
            search_text: [
                "google"
            ],
            tag: `
                <div>
                    <a href="https://www.google.com" target="_blank">Google</a>
                </div>
            `
        },
        {
            id: 4,
            type: "tag",
            text: "Button",
            search_text: [
                "button"
            ],
            tag: `
                <button>button</button>
            `
        },
        {
            id: 5,
            type: "tag",
            text: "Dog",
            search_text: [
                "dog"
            ],
            tag: `
                <img src="./assets/images/dog.jpg" width="150"/>
            `
        },
        {
            id: 6,
            type: "tag",
            text: "Rex",
            search_text: [
                "rex"
            ],
            tag: `
                <div style="display: flex;align-items: center">
                    <img src="./assets/images/dog.jpg" width="50" height="50" style="border-radius: 50%"/>
                    <p style="margin-left: 10px;color: #5e72e4">Rex</p>
                </div>
            `
        },
    ];

    let select = document.querySelector("#select");
    let drop_box = document.querySelector(".drop-box");
    let drop_box_list = document.querySelector(".drop-box-list");

    let show_list = false;

    document.addEventListener( "click", (event) => {
        let isClickInside = drop_box.contains(event.target);
        if (!isClickInside) {
            drop_box_list.style.display = "none";
            drop_box.classList.add("drop-box-btn");
        } else {
            drop_box.classList.remove("drop-box-btn");
            select.focus();
            if (!show_list) {
                select.type = "text";
                select.value = "";
            }
            if (select.value.length > 0)
                drop_box_list.style.display = "grid";
            select.type = "text";
        }
    });
    select.addEventListener( "click", () => {
        if (!show_list) {
            select.type = "text";
            select.value = "";
        } else {
            drop_box_list.style.display = "grid";
        }
    });
    select.addEventListener("blur", () => {
        if (!show_list) {
            select.type = "button";
            select.value = "Dropbox";
            drop_box_list.style.display = "none";
        }
    });
    select.addEventListener( "input", input);

    function input() {
        let select_value = select.value.replace(/ /g,'').toLowerCase();
        if (select_value.length > 0) {
            show_list = true;
            drop_box_list.style.display = "grid";
            drop_box_list.innerHTML = "";
            let search_options = new Set();
            options.forEach( (val, key) => {
                val.search_text.filter( val2 => {
                    if (val2.includes(select_value)) {
                        search_options.add(val)
                    }
                });
            });
            search_options = Array.from(search_options);
            if (search_options.length > 0) {
                search_options.forEach( val => {
                    let div = document.createElement("div");
                    div.classList = "drop-box-option";
                    div.innerHTML = val.tag;
                    div.addEventListener( "click", () => {
                        select.value = val.text;
                        input()
                    });
                    drop_box_list.appendChild(div);
                })
            } else  {
                let div = document.createElement("div");
                div.classList = "drop-box-option";
                div.innerHTML = "Не найдено";
                drop_box_list.appendChild(div)
            }
        } else {
            show_list = false;
            drop_box_list.style.display = "none";
        }
    }
};